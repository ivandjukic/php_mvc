<?php

namespace App\Controller;

use Core\Controller\ControllerBase as Controller;
use App\Model\TestModel as TestModel;


class TestController extends Controller
{
    function show(){
        $test_model = new TestModel();
        $users = $test_model->all();
        $this->view('testview', ['users' => $users]);
    }
}