<?php

namespace App\Model;

use Core\Model\ModelBase as ModelBase;

class TestModel extends ModelBase
{
    protected $table_name = 'users';
}