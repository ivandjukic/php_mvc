<?php
namespace App;

class Routes 
{
    public static $available_routes = [

        [
            'method' => 'get',
            'route' => '/test',
            'controller' => 'TestController',
            'method' => 'test',
        ],
        
        [
            'method' => 'get',
            'route' => '/show',
            'controller' => 'TestController',
            'method' => 'show',
        ],
        // add custom routes  ...



    ];
}