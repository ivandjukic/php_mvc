<?php

namespace Core\Controller;

use Core\View\ViewBase as View;

abstract class ControllerBase
{
    
    protected function view($file_name, $params = [])
    {
        $controller_name = get_called_class();
        $view_directory = str_replace("App\Controller\\", "", $controller_name);
        $view_directory = str_replace("Controller", "", $view_directory);

        $file = $view_directory . "/" . $file_name . '.php';
        
        View::renderPage($file, $params);
    }
   
}