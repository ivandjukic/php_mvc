<?php

namespace Core\Database;

use Config\Config as Config;

class DBAL
{
    protected $db;
   
    public function __construct() {
        try{
            $this->connectToServer();
        }
        catch(\Exception $e) {
            // handle exception
        }
    }
    
    protected function connectToServer() {
        $config = Config::$database;
        $hostname = $config['hostname'] . ":" . $config['port'];
            $link = mysqli_connect($hostname, $config['username'], $config['password'], $config['database']);
            if (mysqli_connect_errno()){
                throw new \Exception("Failed to connect to MySQL: " . mysqli_connect_error());
            } else {
                $this->db = $link;
            }
    }
    

    public function selectQuery($sql) {
        if(!empty(trim($sql))){
            $data = array();
            $result = mysqli_query($this->db, $sql);
            while ($obj = mysqli_fetch_object($result)){
                $data[] = $obj;
            }
            return $data;
        }
    }

    public function insertQuery($sql) {
        $result = mysqli_query($this->db, $sql);
        return $result;
    }

    public function deleteQuery($sql) {
        $result = mysqli_query($this->db, $sql);
        return $result;
    }
    
}