<?php

namespace Core\Model;

use Core\Database\DBAL as dbal;
use App\Config;

abstract class ModelBase
{
    protected $table_name = null;

    function __construct() 
    {
        $child_model_class = get_called_class();
        $this->table_name = $this->getTableName($child_model_class);
    }
    

    function  getTableName($child_model_class) 
    {
        $child_model_vars = get_class_vars($child_model_class);
        if($child_model_vars['table_name']) {
            return $child_model_vars['table_name'];
        } else {
            $table_name = str_replace('App\Model\\', '', $child_model_class);
            $table_name = str_replace('Model', '', $table_name);
            $table_name = strtolower($table_name);
            return $table_name . 's';
        }
    }

    function find($id) 
    {
        $db = new DBAL();  
        $table_name = $this->$table_name;
        $result = $db->selectQuery("SELECT * FROM $table_name WHERE id=$id");
        return $result[0];
    }

    function all() 
    {
        $db = new DBAL();  
        $table_name = $this->table_name;
        $result = $db->selectQuery("SELECT * FROM $table_name");
        return $result;
    }   

    function save() 
    {
        $db = new DBAL();  
        $table_name = $this->table_name;
        $query = "INSERT INTO $table_name VALUES(";
        foreach($this as $attribute_name => $attribute_value) {
            if($attribute_name != "table_name" && $attribute_name != "") {
                $query .= "\"$attribute_value\"";
            }
        }
        $query .= ");";
        $result = $db->insertQuery($query);
        return $result;
    }

    function delete($id) 
    {
        $db = new DBAL();  
        $table_name = $this->table_name;
        $query = "DELETE FROM $table_name WHERE id = $id";
        $result = $db->delete($query);
        return $result;
    }


}
