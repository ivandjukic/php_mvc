<?php

namespace Core\View;

class ViewBase
{
    public static function renderPage($file_path, $params = [])
    {
        extract($params, EXTR_SKIP);
        $file = dirname(__DIR__) . "/App/View/$file_path";  
        $file = str_replace('/Core', '', $file);
        if (is_readable($file)) {
            require $file;
        } else {
            throw new \Exception("$file not found");
        }
    }
   
}